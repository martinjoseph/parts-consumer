package com.kubota.kl.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.kubota.kl.dealermeet.parts.dto.EMailDTO;
import com.kubota.kl.dealermeet.parts.dto.hbm.OrderDTO;
import com.kubota.kl.dealermeet.parts.exception.DealerMeetingPartsException;
import com.kubota.kl.dealermeet.parts.message.OrderDTODeserializer;
import com.kubota.kl.dealermeet.parts.services.EmailServiceBean;
import com.kubota.kl.dealermeet.parts.services.OrderServiceBean;
import com.kubota.kl.dealermeet.parts.services.SAPServiceBean;
import com.kubota.kl.dealermeet.parts.util.ConfigUtil;
import com.kubota.kl.dealermeet.parts.util.Constants;
import com.fasterxml.jackson.databind.JsonNode;


@SpringBootApplication
public class SpringKafkaApplication {
	public static void main(String[] args) {
		 Properties props = new Properties();
		    props.put("bootstrap.servers", "localhost:9092");
		    props.put("group.id", "group-1");
		    props.put("enable.auto.commit", "true");
		    props.put("auto.commit.interval.ms", "1000");
		    props.put("auto.offset.reset", "earliest");
		    props.put("session.timeout.ms", "30000");
		    props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		    props.put("value.deserializer", "com.kubota.kl.dealermeet.parts.message.OrderDTODeserializer");

		    KafkaConsumer<String, OrderDTO> kafkaConsumer = new KafkaConsumer<>(props);
		    kafkaConsumer.subscribe(Arrays.asList("OrderTopic"));
		    while (true) {
		      ConsumerRecords<String, OrderDTO> records = kafkaConsumer.poll(100);
		      for (ConsumerRecord<String, OrderDTO> record : records) {
		    	  OrderDTO orderDTO =(OrderDTO) record.value();
		    	  
		    	  
		    	  System.out.println("Receive: " + orderDTO.toString());
		    	  
		    	  SAPServiceBean sapService = SAPServiceBean.getInstance();
					try {
						orderDTO = sapService.submitOrderToSAP(orderDTO);
					} catch (DealerMeetingPartsException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					OrderServiceBean orderService = OrderServiceBean.getInstance();
					try {
						orderService.saveOrder(orderDTO);
					} catch (DealerMeetingPartsException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					if( orderDTO.getStatusCd().equals(Constants.ORDER_STATUS_CODES.SAP_VALIDATION_FAIL) || 
							orderDTO.getStatusCd().equals(Constants.ORDER_STATUS_CODES.SAP_ERROR) )
							sendErrorEmail(orderDTO);
		      }
		    }
	  }
	
	
	
	static private void sendErrorEmail(OrderDTO orderDTO) {
		EmailServiceBean emailService = new EmailServiceBean();
		try {
			String fromAddress = new ConfigUtil().getConfig().getEmailFromAddress();
			ArrayList<String> toAddressList = new ConfigUtil().getConfig().getEmailToAddressList();
			EMailDTO emailDTO = new EMailDTO();
			emailDTO.setFromAddress(fromAddress);
			emailDTO.setToAddress(toAddressList);
			emailDTO.setSubject("Dealer Meeting Parts Order Error: " + Integer.toString(orderDTO.getDealerId()));
			
			String body = "";
			body += "SAP Error Occurred For Order ID: ";
			body += Integer.toString(orderDTO.getOrderId()) + "\n";
			body += "Order Status: " + orderDTO.getStatusCd();
			emailDTO.setBody(body);
			
			ArrayList<String> attachmentList = new ArrayList<String>();
			attachmentList.add(orderDTO.getOutputFileName() + "_resp.xml");
			emailDTO.setAttachementList(attachmentList);
			
			emailService.createAndSendMail(emailDTO);
			
		} catch(Exception ex) {
			ex.printStackTrace();
			System.out.println("Exception: " + ex.getMessage());
		}
	}
	

}
