package com.kubota.kl.parts.messaging;

import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;

import com.kubota.kl.dealermeet.parts.message.OrderDTODeserializer;

public class PartsConsumer {
	 private static final Logger LOGGER =
		      LoggerFactory.getLogger(PartsConsumer.class);

		  private CountDownLatch latch = new CountDownLatch(1);

		  public CountDownLatch getLatch() {
		    return latch;
		  }

		  @KafkaListener(topics = "${kafka.topic.parts}")
		  public void receive(OrderDTODeserializer payload) {
		    LOGGER.info("received payload='{}'", payload);
		    latch.countDown();
		  }
}
